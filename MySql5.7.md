## MySql5.7

### 添加用户

```mysql
CREATE USER 'username'@'%' IDENTIFIED BY 'password';
```

### 授权访问

```mysql
GRANT ALL privileges ON *.* TO 'username'@'%';
```

### 关闭ROOT远程访问

```mysql
delete from user where user="root" and host="%";
```



### 刷新权限

```mysql
flush privileges;
```

