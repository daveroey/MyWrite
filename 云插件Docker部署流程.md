# 云插件Docker部署流程

## 一、在本地电脑或服务器上安装docker

>windows安装：https://docs.docker.com/docker-for-windows/install/
>
>mac osx安装：https://docs.docker.com/docker-for-mac/install/
>
>centos安装：https://docs.docker.com/install/linux/docker-ce/centos/
>
>ubuntu安装(/https://docs.docker.com/install/linux/docker-ce/ubuntu/

## 二、构建Docker镜像

1. 将工程打包为`angel.war`

2. Dockerfile文件编写，创建文件为`Dockerfile`，拷贝以下内容。

   ```dockerfile
   #轻量alpine镜像
   FROM tomcat:8.0-jre8-alpine
   
   MAINTAINER Dave <389788728@qq.com>
   
   #工作目录
   ENV WORK_DIR /usr/local/tomcat
   #war名称
   ENV DEPLOY_WAR_NAME angel.war
   #时区
   ENV  TIME_ZONE Asia/Shanghai
   #jvm参数
   ENV JAVA_OPTS '-server -Xms2048m -Xmx2048m -XX:PermSize=1024M -XX:MaxPermSize=512m'
   
   #设置时区、清除tomcat默认文件
   RUN apk add --no-cache tzdata \
   && cp -r -f /usr/share/zoneinfo/$TIME_ZONE /etc/localtime \
   && rm -rf $WORK_DIR/webapps/
   
   #复制包
   COPY  ./$DEPLOY_WAR_NAME $WORK_DIR/webapps/
   
   
   ```

3. 将`Dockerfile`,`angel.war`,放到同一个目录下。执行 `docker build -t tomcat:tag .`命令构建镜像。（tag指定版本号，注意后面有个 ''*.*"）

### 三、本地运行测试

```shell
docker run --name tomcat -dit -p 8080:8080 <tomcat:tag>
```

### 四、按云插件文档上传部署

> 端口映射使用默认的8080端口，负载的9090默认指向了8080。

### 五、测试部署

>地址：http://171.208.222.146:6041/angel/