

## [Spring Batch](/https://docs.spring.io/spring-batch/4.0.x/reference/html/index-single.html)

## 1 使用场景

1. 典型的批处理程序一般：
   - 从数据库，文件或队列中读取大量记录。
   - 以某种方式处理数据。
   - 以修改的形式写回数据。

2. 使用Spring batch的业务场景
   - 周期性的批处理作业
   - 并发批处理：并行处理作业
   - 分阶段的企业消息驱动处理
   - 大规模并行批处理
   - 失败后手动或预定重启
   - 顺序处理依赖的步骤（使用扩展的批处理工作流）
   - 部分处理：跳过记录（例如，在回滚时）
   - 整批交易，适用于批量较小或现有存储过程/脚本的情况

## 2 Spring batch的整体架构

![Spring batch架构图](https://docs.spring.io/spring-batch/4.0.x/reference/html/images/spring-batch-reference-model.png)

> `Job`由一个到多个`Step`（步骤）组成，每个步骤只有一个`ItemReader`，一个`ItemProcessor`和一个`ItemWriter`。一个Job的启动的对象 `JobLauncher`，并且需要存储有关当前正在运行的进程的元数据对象`JobRepository`。



## 3  Job

​	Job封装了整个批处理流程的细节，是整个作业流程的顶层。Job的层次结构如下。

![Job层次结构](https://docs.spring.io/spring-batch/4.0.x/reference/html/images/job-heirarchy.png)

> Job是多个步骤的组成的实例对象，作用是为多个步骤配置全局的可配置属性。
>
> - 工作的名称
> - 每一个Step的定义配置、Step执行的顺序。
> - Step失败处理（重试处理）

​	Spring Batch已经默认为我们实现了一个SimpleJob,我们可以使用对应的构建器`JobBuilderFactory`来创建一个Job

```java
@Autowired
private JobBuilderFactory jobBuilderFactory;

@Bean
public Job footballJob() {
    return this.jobBuilderFactory.get("footballJob")
                     .start(playerLoad())
                     .next(gameLoad())
                     .next(playerSummarization())
                     .end()
                     .build();
}
```

## 3.1 . JobInstance

​	`JobInstance`我理解的是`Job`每次执行所产生的实例，Job在运行的时候有个对应的`JobParameters`对象,它包含了一组用户Job启动的参数。可以看成：`JobInstance`=`Job`+`JobParameters`。



## 3.2 . JobParameters

​	一个`JobParameters`对象包含了一组用于启动批处理作业参数。它们可以在运行期间用于识别甚至作为参考数据，如下图所示：

![Job 层级图](https://docs.spring.io/spring-batch/4.0.x/reference/html/images/job-stereotypes-parameters.png)

> 比如可以通过`JobParameters`控制执行的日期来区分不同的`JobInstance`，这样可以有效的控制Job的定义、方便监控Job的执行情况。



## 3.3 . JobExecution

​	`JobExecution`是`Job`运行期间实际发生的情况的主要存储机制，针对持久化策略**持久化执行的结果**(jdbc、map等)。并且包含了很多需要持久化的属性,记录了Job的执行状态，如果需要重试任务，可以找回需要重试的`JobInstance`。

> 具体的持久化属性请参见 [官方文档](/https://docs.spring.io/spring-batch/4.0.x/reference/html/index-single.html)



## 4 . Step

​		`Step` 封装了批处理作业的独立顺序阶段,每个`Job`由一个或多个`Step`组成	。`Step`包含定义和控制实际批处理所需的所有信息，与一个`Job`相同。一个`Step`有一个与它**唯一相关的**`StepExecution`。

![Step 层级图](https://docs.spring.io/spring-batch/4.0.x/reference/html/images/jobHeirarchyWithSteps.png)

## 4.1 StepExecution

​		和`JobExecution`类似，每次执行`Steps`都会创建一个`StepExecution`,当一个Job中前面的Step执行失败，导致后面的`Steps`没有执行那么``StepExecution``不会被创建。`StepExecution`中包含了对应`Step`的信息，和`JobExecution`相关的数据引用，例如提交和回滚计数以及开始和结束时间。

​	每个`StepExecution`都包含一个上下文对象`ExecutionContext`,包含开发人员需要在批处理运行中保留的任何数据，例如重新启动所需的统计信息或状态信息。

> StepExecution的持久化属性详见  [官方文档](/https://docs.spring.io/spring-batch/4.0.x/reference/html/index-single.html)



## 4.2 . ExecutionContext

​	`ExecutionContext`是一组键值的集合，可以由开发者指定数据。官方文档有个按行处理文件的例子：

	>​     	有一个按行处理文件的`Step`，它执行了3000行失败了，我们可以把处理的行数记录保存在`ExecutionContext`中，当重试这个`Step`时可以读取出`ExecutionContext`中的数据，从上次的记录处开始继续执行。
	>
	>```java
	>//记录数据
	>executionContext.putLong(getKey(LINES_READ_COUNT), reader.getPosition());
	>```
	>
	>```java
	>//从上次执行的地方继续执行
	>if (executionContext.containsKey(getKey(LINES_READ_COUNT))) {
	>    log.debug("Initializing for restart. Restart data is: " + executionContext);
	>
	>    long lineCount = executionContext.getLong(getKey(LINES_READ_COUNT));
	>
	>    LineReader reader = getReader();
	>
	>    Object record = "";
	>    while (reader.getPosition() < lineCount && record != null) {
	>        record = readLine();
	>    }
	>}
	>```
	>
	>
	>
	>​	但是如果Step的`JobInstance`是不同的（上面提到JobInstance由JobParameters与Job组成），那么是取不到上下文对象的。

## 5.  JobRepository

​	`JobRepository`是上面提到的所有对象的持久化机制。它提供了CRUD操作`JobLauncher`，`Job`以及`Step`的实现。当 一个`Job`第一次启动，一个`JobExecution`将会被`JobRepository`创建，并且，执行的过程中，`StepExecution`和`JobExecution`被持续持久化到`JobRepository`。



## 6.  JobLauncher

`JobLauncher`表示：用给定的`JobParameters`集合,启动`Job`.

```java
//api
public interface JobLauncher {

public JobExecution run(Job job, JobParameters jobParameters)
            throws JobExecutionAlreadyRunningException, JobRestartException,
                   JobInstanceAlreadyCompleteException, JobParametersInvalidException;
}
```

## 7. ItemReaders和ItemWriters

​	所有批处理都可以用最简单的形式描述为读取大量数据，执行某种类型的计算或转换，以及将结果写出来。Spring Batch的提供了三个关键的接口，以帮助执行批量处理读取和写入： `ItemReader`，`ItemProcessor`，和`ItemWriter`。



## 7.1 . ItemReader

​	`ItemReader` 默认提供了多个实现，可以从多种输入类型获得数据。具体参见：[ItemReader实现](/https://docs.spring.io/spring-batch/4.0.x/reference/html/index-single.html#listOfReadersAndWriters)

- 从文件获得
- 从XML获得
- 从数据库获得
- 从消息队列获得
- 等等。。。

```java
//api 提供read方法
public interface ItemReader<T> {

    T read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException;

}
```



###  7.1 . ItemWriter

​	`ItemWriter`功能类似于`ItemReader`但具有反向操作。在数据库或队列的情况下，这些操作可能是插入，更新或发送。为每个批处理任务输出序列化的特定各式数据。

```java
public interface ItemWriter<T> {

    void write(List<? extends T> items) throws Exception;

}
```

## 7.2 ItemProcessor

​	`ItemProcessor`的作用是：给定一个对象，转换它并返回另一个对象。提供的对象可以是同一个类型也可以不是同一类型。关键是业务逻辑可以在流程中应用，完全由开发人员来创建逻辑。

```java
public class Foo {}

public class Bar {
    public Bar(Foo foo) {}
}

public class FooProcessor implements ItemProcessor<Foo,Bar>{
    public Bar process(Foo foo) throws Exception {
        //Perform simple transformation, convert a Foo to a Bar
        return new Bar(foo);
    }
}

public class BarWriter implements ItemWriter<Bar>{
    public void write(List<? extends Bar> bars) throws Exception {
        //write bars
    }
}
```

可以将多个`ItemProcessors`连接在一起组成一个`CompositeItemProcessor`，实现多个转换。

```java
public class Foo {}

public class Bar {
    public Bar(Foo foo) {}
}

public class Foobar {
    public Foobar(Bar bar) {}
}

//Foo转换为Bar
public class FooProcessor implements ItemProcessor<Foo,Bar>{
    public Bar process(Foo foo) throws Exception {
        //Perform simple transformation, convert a Foo to a Bar
        return new Bar(foo);
    }
}

//Bar转为Foobar
public class BarProcessor implements ItemProcessor<Bar,Foobar>{
    public Foobar process(Bar bar) throws Exception {
        return new Foobar(bar);
    }
}

public class FoobarWriter implements ItemWriter<Foobar>{
    public void write(List<? extends Foobar> items) throws Exception {
        //write items
    }
}
```

将两个`ItemProcessors`连接在一起。

```java
CompositeItemProcessor<Foo,Foobar> compositeProcessor =
                                      new CompositeItemProcessor<Foo,Foobar>();
List itemProcessors = new ArrayList();
itemProcessors.add(new FooTransformer());
itemProcessors.add(new BarTransformer());
compositeProcessor.setDelegates(itemProcessors);
```



## 7.3 过滤处理记录

`ItemProcessor`可以对传递给`ItemWriter`的集合进行过滤，跳过那些无效的记录。可以从`ItemProcessor`的处理结果处返回*NULL*来过滤结果。



## 8  ItemStream

`ItemStream`用于协调`ItemReaders`与`ItemWriters`,用于处理两者的状态持久化，确保两者打开的资源安全释放。

```java
public interface ItemStream {

    void open(ExecutionContext executionContext) throws ItemStreamException;

    void update(ExecutionContext executionContext) throws ItemStreamException;

    void close() throws ItemStreamException;
}
```



## 9  Flat Files

​	平面文件：具有某种各式的文件。所有平面文件分为两种类型：**特定符号分隔**和**固定长度**。分隔文件是字段由分隔符分隔的文件，例如逗号。固定长度文件具有设定长度的字段。

## 9.1 FieldSet

​	`FieldSet`类似于JDBC中的`ResultSet`，Spring Batch中`FieldSet`包含文件读取的抽象,提供了对平面文件处理的一致解析。可以通过索引或者通过配置的列名读取。

```java
        String[] tokens = new String[]{"1001", "dave", "true"};
        String[] names=new String[]{"id","name","gender"};
        FieldSet fs = new DefaultFieldSet(tokens,names);
        String id = fs.readString(0);
        String name = fs.readString("name");
        boolean booleanValue = fs.readBoolean(2);
```

## 9.2 FlatFileItemReader

​	`FlatFileItemReader`是`ItemReader`的一个实现，提供了读取和解析平面文件的基本功能。它有两个重要的依赖类，[Resource](/https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#resources)和[LineMapper](/https://docs.spring.io/spring-batch/4.0.x/reference/html/index-single.html#lineMapper)。

**注意：**`FlatFileItemReader `初始化时，它会打开文件（如果存在），如果不存在则抛出异常。

## 9.3  LineMapper

​	类似于Jdbc的`RowMapper`,不过`RowMapper`的构造是传入一个`ResultSet`和`lineNumber`, `LineMapper`将String转换为一个Object，并与对应的`lineNumber（行号）`相关联，比如从文件读取到一个字段，需要转换为`int`类型。

```java
public interface LineMapper<T> {

    T mapLine(String line, int lineNumber) throws Exception;

}
```

## 9.4  LineTokenizer

​	将输入行转换为一个`FieldSet`，在Spring Batch中，通过`LineTokenizer`将许多格式的平面文件数据转换为 `FieldSet`。

```java
public interface LineTokenizer {

    FieldSet tokenize(String line);

}
```

输入行可能是带有**分隔符**、**特定长度字段**、或者特定的**匹配模式**。所以`LineTokenizer`提供了几个实现。

- `DelimitedLineTokenizer`: **处理特定符号分割的行**
- `FixedLengthTokenizer`: **每行中固定宽度的字段**
- `PatternMatchingCompositeLineTokenizer`：**同过对匹配模式的check确定是哪个**



## 9.5  FieldSetMapper

​	`FieldSetMapper`提供了一个`mapFieldSet `方法，将一个`FieldSet`映射到一个实体对象，具体对象实现取决于业务，注意区别于`LineMapper`对象，后者是针对分割的单个字段。

```java
public interface FieldSetMapper<T> {

    T mapFieldSet(FieldSet fieldSet) throws BindException;

}
```



## 9.6  DefaultLineMapper

​	`DefaultLineMapper` 提供了`LineMapper`的一个简单实现：

```java
public class DefaultLineMapper<T> implements LineMapper<>, InitializingBean {

    private LineTokenizer tokenizer;

    private FieldSetMapper<T> fieldSetMapper;

    public T mapLine(String line, int lineNumber) throws Exception {
        return fieldSetMapper.mapFieldSet(tokenizer.tokenize(line));
    }

    public void setLineTokenizer(LineTokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    public void setFieldSetMapper(FieldSetMapper<T> fieldSetMapper) {
        this.fieldSetMapper = fieldSetMapper;
    }
}
```

>需要三个基本步骤：
>
>- 从文件中读取一行`String`
>- 将该`String`行传递给`LineTokenizer#tokenize()`方法以检索a `FieldSet`
>- 将`FieldSet`返回的标记化传递给a `FieldSetMapper`，从`ItemReader#read()`方法返回结果。

## 9.7 FlatFileItemReader使用小实例

​	[官方的PlayFileItemReader实现](/)





## 10 FlatFileItemWriter

​	将内容输出到平面文件，使用到了`FlatFileItemWriter`它是`ItemWriter`对于平面文件的实现，实现文件的写入也需要将处理后的列表转换为特定的各式，**特定的符号分割**、或者**固定的长度**的**String**.

**注意：**`FlatFileItemWriter` 包含属性`shouldDeleteIfExists`。将此属性设置为true在打开Writer时时会删除具有相同名称的现有文件。



## 10.1  LineAggregator

​	与`LineTokenizer` 相反的，`LineAggregator`是将多个字段聚合成一定各式的**String**的聚合器。

```java
public interface LineAggregator<T> {

    public String aggregate(T item);

}
```

## 10.2 PassThroughLineAggregator

​	`LineAggregator`接口的一个最基本实现是 `PassThroughLineAggregator`，调用对象的**toString()**方法得到组合的**String**.前提是必须重写对象的**toString()**方法，不然将变得毫无意义。

```java
public class PassThroughLineAggregator<T> implements LineAggregator<T> {

    public String aggregate(T item) {
        return item.toString();
    }
}
```

>`LineAggregator` 的其他实现：
>
>- [DelimitedLineAggregator](/https://docs.spring.io/spring-batch/4.0.x/reference/html/index-single.html#delimitedFileWritingExample) 分隔符聚合器
>- [FormatterLineAggregator](/https://docs.spring.io/spring-batch/4.0.x/reference/html/index-single.html#fixedWidthFileWritingExample) 固定宽度聚合器，具体的分割format语法可以[点这里查看](/https://docs.oracle.com/javase/8/docs/api/java/util/Formatter.html).



## 10.3  FieldExtractor

​	有时候需要将一个特定的对象转换成一行再写入，Spring Batch无法知道要写入的**对象中有哪些字段**，所以需要将要写入的`Item`对象转换成数组，然后通过**特定符号**，或者**固定长度**生成一个`String` 。

```java
public interface FieldExtractor<T> {

    Object[] extract(T item);

}
```

此时，写入流程和读取流程正好相反。

> 1. 将要写入的项目传递给ItemWriter。
> 2. 将特定对象Item上的字段转换为数组。
> 3. 将结果数组聚合成一行。

## 10.4 PassThroughFieldExtractor

​	`PassThroughFieldExtractor` 是`FieldExtractor` 的一个实现。

```java
//api
public class PassThroughFieldExtractor<T> implements FieldExtractor<T> {
    public PassThroughFieldExtractor() {
    }

    public Object[] extract(T item) {
        if (item.getClass().isArray()) {
            return (Object[])((Object[])item);
        } else if (item instanceof Collection) {
            return ((Collection)item).toArray();
        } else if (item instanceof Map) {
            return ((Map)item).values().toArray();
        } else {
            return (Object[])(item instanceof FieldSet ? ((FieldSet)item).getValues() : new Object[]{item});
        }
    }
}
```

​	从源码中可以看到，支持 `数组`，`集合` ，`Map`,甚至是`FieldSet`。但是传入的对象不是以上类型，则会返回一个`new Object[]{item}`,即将集合转换为数组。

## 10.5 BeanWrapperFieldExtractor

​	`BeanWrapperFieldExtractor` 类似于`BeanWrapperFieldSetMapper` ,它的作用是：将一个对象转换为对象数组，而不需要自己手动转换。

```java
BeanWrapperFieldExtractor<Name> extractor = new BeanWrapperFieldExtractor<Name>();
//设置映射的FiledName,根据对象上的setter属性去映射。
extractor.setNames(new String[] { "first", "last", "born" });

String first = "Alan";
String last = "Turing";
int born = 1912;

Name n = new Name(first, last, born);
Object[] values = extractor.extract(n);
```

​	**注意：** 名称的顺序决定了数组中字段的顺序，`extractor.setNames(new String[] { "first", "last", "born" })`,文件的字段顺序可以通过这个定义。

## 10.6 文件聚合使用实例

​	[文件输出聚合使用实例](/)



## 11 多文件读取MultiResourceItemReader

​	由于业务需要，需要在一个`Step`中处理多个文件，且这些文件具有**相同的各式**，**一定的命名规范**，可以使用通配符去读取。

```shell
文件的各式
file-1.txt file-2.txt ignored.txt	
```

```xml
<bean id="multiResourceReader" class="org.spr...MultiResourceItemReader">
    <property name="resources" value="classpath:data/input/file-*.txt" />
    <property name="delegate" ref="flatFileItemReader" />
</bean>
```



## 12  数据库的ItemReader

​	业务基本都是与数据库打交道，批处理通常都是返回大量数据集。Spring Batch提供了两种类型的解决方案。

- 基于游标的`ItemReader`实现
- `ItemReader` 的分页方式实现

## 12.1 基于游标的ItemReaders

​	大多数批处理开发都采用基于游标的方式，类似于`JDBC`的`ResultSet` ，将一个**游标**维护到当前数据行，调用`next()`将游标移到下一行。Spring Batch基于游标的`ItemReader` 实现在初始化时打开一个游标，每次调用`read()`都会向前移动一个游标，返回一个可用于处理的映射对象。类似下图：

![æ¸¸æ ç¤ºä¾](https://docs.spring.io/spring-batch/4.0.x/reference/html/images/cursorExample.png)

## 12.1.1  JdbcCursorItemReader

​	`JdbcCursorItemReader`是基于游标的技术的JDBC实现，它的工作方式和`ResultSet`相同，需要从一个数据源获得连接，并通过`SQL`去执行。最大的优点是支持`流式读取`，一旦调用`read()`方法，可以同时使用`ItemWriter`写出，`read()`会继续读取下一个Item.

```java
@Autowired
    private DataSource dataSource;

    public static final String ID_COLUMN = "id";
    public static final String NAME_COLUMN = "name";
    public static final String CREDIT_COLUMN = "credit";
    
    @Bean
    public JdbcCursorItemReader<CustomerCredit> itemReader() {
        return new JdbcCursorItemReaderBuilder<CustomerCredit>()
                .dataSource(this.dataSource)
                .name("creditReader")
                .sql("select ID, NAME, CREDIT from CUSTOMER")
                .rowMapper(customerCreditRowMapper())
                .fetchSize(Integer.MIN_VALUE)
                .build();

    }

    @Bean
    RowMapper<CustomerCredit> customerCreditRowMapper() {
        return (rs, i) -> {
            CustomerCredit customerCredit = new CustomerCredit();
            customerCredit.setId(rs.getInt(ID_COLUMN));
            customerCredit.setName(rs.getString(NAME_COLUMN));
            customerCredit.setCredit(rs.getBigDecimal(CREDIT_COLUMN));
            return customerCredit;
        };
    }

```

关于`JdbcCursorItemReader`的设置属性，[点我查看](/https://docs.spring.io/spring-batch/4.0.x/reference/html/index-single.html#JdbcCursorItemReader)

## 12.1.2  HibernateCursorItemReader

​	`HibernateCursorItemReader`是**Cursor**的Hibernate实现,提供`ORM`的解决方案。

```java
@Bean
public HibernateCursorItemReader itemReader(SessionFactory sessionFactory) {
        return new HibernateCursorItemReaderBuilder<CustomerCredit>()
                        .name("creditReader")
                        .sessionFactory(sessionFactory)
                        .queryString("from CustomerCredit")
                        .build();
}
```



## 12.1.3 StoredProcedureItemReader

​	使用**存储过程**获取游标数据。 `StoredProcedureItemReader`工作方式类似`JdbcCursorItemReader`，只是运行方式由SQL变成了存储过程，存储过程可以以三种不同的方式返回游标数据：

- 返回一个 `ResultSet`（由SQL Server，Sybase，DB2，Derby和MySQL使用）
- 由一个out参数`ref-cursor`返回（由Oracle和PostgreSQL使用）
- 通过一个函数返回

```java
//返回ResulSet实例
@Bean
public StoredProcedureItemReader reader(DataSource dataSource) {
        StoredProcedureItemReader reader = new StoredProcedureItemReader();

        reader.setDataSource(dataSource);
        reader.setProcedureName("sp_customer_credit");
        reader.setRowMapper(new CustomerCreditRowMapper());

        return reader;
}
```

其他的两种方式参见：[点我查看](/https://docs.spring.io/spring-batch/4.0.x/reference/html/index-single.html#StoredProcedureItemReader)



## 12.2 分页方式的ItemReader

​	通通指定查询的**起始位置**，和**要返回的行数 **，返回数据。

## 12.2.1   JdbcPagingItemReader

​	`JdbcPagingItemReader` 是 `ItemReader` 的分页实现方式，需要一个`PagingQueryProvider`负责构建查询的SQL。由于每个数据库都有自己的提供分页支持的策略，因此我们需要`PagingQueryProvider` 为每个支持的数据库类型使用不同的数据库。还有`SqlPagingQueryProviderFactoryBean` 自动检测正在使用的数据库并确定适当的 `PagingQueryProvider`实现。

```java
@Autowired
    private CustomerCreditMapper customerCreditMapper;
    @Autowired
    DataSource dataSource;

    @Bean
    public JdbcPagingItemReader itemReader() throws Exception {
        //设置参数值
        Map<String, Object> parameterValues = new HashMap<>();
        parameterValues.put("status", "NEW");
        //以传统的?占位符方式，键为索引 1开始
        //parameterValues.put("1", "NEW");

        return new JdbcPagingItemReaderBuilder<CustomerCredit>()
                .name("creditReader")
                .dataSource(dataSource)
                .queryProvider(queryProvider())
                .parameterValues(parameterValues)
                .rowMapper(customerCreditMapper)
                .pageSize(1000)//每次从数据库返回的数据条数
                .build();
    }

    @Bean
    public PagingQueryProvider queryProvider() throws Exception {
        SqlPagingQueryProviderFactoryBean provider = new SqlPagingQueryProviderFactoryBean();

        provider.setSelectClause("select id, name, credit");
        provider.setFromClause("from customer");
        provider.setWhereClause("where status=:status");//也可以使用传统的?占位符，
        provider.setSortKey("id");
        return provider.getObject();
    }
```



## 12.2.2  JpaPagingItemReader

​	`JpaPagingItemReader` 是`ItemReader` 分页的另一种实现方式。声明一个**JPQL**语句，并传递一个 `EntityManagerFactory`。前提是需要配置实体对象的**JPA注释**或者**ORM映射文件**，[JPA参考指南](/https://docs.spring.io/spring-data/jpa/docs/2.1.1.RELEASE/reference/html/)

```java
@Autowired
    private EntityManagerFactory entityManagerFactory;

    @Bean
    public JpaPagingItemReader itemReader() {
        return new JpaPagingItemReaderBuilder<CustomerCredit>()
                .name("creditReader")
                .entityManagerFactory(entityManagerFactory)
                .queryString("select c from CustomerCredit c")
                .pageSize(1000)
                .build();
    }
```

## 13 数据库的ItemWriters

​	数据库的ItemWriters，官方的建议是自己实现`ItemWrite`接口，一般业务都是与数据库打交道，这方面各种框架已经有了完美的实现。官方也提供了些实现：

- `JdbcBatchItemWriter`
- `JpaItemWriter`
- `RepositoryItemWriter`





