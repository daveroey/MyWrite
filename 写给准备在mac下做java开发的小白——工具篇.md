

# 写给准备在mac下做java开发的小白——工具篇

***

## java开发神器 [Intellij IDEA](https://www.jetbrains.com/idea/)

 jetbrains出品，必属精品。无论是`PyChar`、`WebStorm`、`PhpStorm`甚至是现在作为安卓第一开发语言的`kotlin` 都深受开发者喜爱。

IDEA中集成了很多实用的插件、比如spring支持插件、data jpa方法命名提示、配置文件导航等。如果不想去折腾Eclipse插件，那么毫无疑问IDEA是最好的选择，如果你觉得MyEclipse更好用 那么就当我没说。

![idea](/Users/mac/Documents/image-20180606173111710.png)

***



## Mac 终端利器[iTerm2](https://www.iterm2.com)

​    iTerm2是公认的mac下最好用的终端工具、支持 `修改配色`、`语法高亮`、`分屏`

`显示`以及强大的`自动补全`。笔者最常用的功能是Profiles。在iterm2里面敲击组合键：`command`+`o ` 就可以打开Profiles的配置页面，可以使用[Expect](https://blog.csdn.net/chinalinuxzend/article/details/1761202)脚本语言写一些自动化的脚本添加到Profies里面，比如自动登录、文件传输等。

[iTerm2配置自动登录远程主机](https://blog.csdn.net/langsim/article/details/50445245)

![a](/Users/mac/Documents/image-20180606175901544.png)

***

## 数据库管理工具[Navicat](https://www.navicat.com/en/products/navicat-for-mysql)

在mac下对比了几个数据库管理工具、笔者最终还是选择了Navicat。可以同时打开多个数据连接、支持美化SQL、已及SQL智能感知等。Navicat可以非常容易的进行跨数据库数据传输、备份恢复已及导出查询结果为xml、word、txt等多种格式。

![image-20180606191426011](/Users/mac/Documents/image-20180606191426011.png)

***

## FTP/SFTP传输工具[ForkLift](https://binarynights.com)

对比了mac下的多款FTP/SFTP工具，最后选择了FrokLift。ForkLift的界面比同类的FTP/SFTP都好看得多，这也是我选择它的重要原因。ForkLift支持同时上传多个文件（可以继续上传其他文件），不会像winscp那样上传文件的时候就不能继续操作了。此外ForkLift还支持 *文件同步*、*文件预览*、*远程编辑*、*本地文件管理*等功能。唯一的一个缺点就是*不支持隧道连接* 每次连接内网都需要借助其他工具打通内网隧道。

![image-20180606194022324](/Users/mac/Documents/image-20180606194022324.png)

***

## 内网隧道工具[SSH Tunnel](https://itunes.apple.com/cn/app/ssh-tunnel/id734418810?mt=12)

许多时候不能直接访问一个 `内网主机` 、需要通过一个 `代理主机` 建立隧道才能访问。mac可以直接使[`命令来创建`](https://www.linuxidc.com/Linux/2012-01/51022.htm)，但是当隧道比较多的时候还是需要一个工具去管理。SSH Tunnel可以将新建的隧道同步到icloud中,可以同时连接几百上千隧道，支持*自动重连*  、*导入导出* 。

![image-20180606201111765](/Users/mac/Documents/image-20180606201111765.png)

***

## HTTP模拟请求工具[Postman](https://www.getpostman.com)

Postman是在网站开发中非常实用的 `模拟请求` 工具，支持get、post、put、文件上传、响应验证等功能。Postman还可以将请求用例生成文档、已及生成各大语言的代码。如果你使用google账号登录Postman,会自动将用例同步到google，好吧这个在国内很鸡肋。

![image-20180606202953390](/Users/mac/Documents/image-20180606202953390.png)

***

