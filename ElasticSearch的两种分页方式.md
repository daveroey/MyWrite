# [ElasticSearch](https://www.elastic.co/products/elasticsearch)的两种分页方式

> 笔者最近在工作中用到了ES,项目需要往ES放入大量数据（每天亿级）然后定期检索每天的数据到FTP中。之前没接触过ES、使用过程中遇到很多问题。在此记录下ES分页读检索数据时遇到的问题。
>
> 

## 准备工作

1. 一台已经安装[ElasticSearch](https://www.elastic.co/products/elasticsearch)的服务器,去官网下载安装即可。

2. 一个[Spring](https://www.elastic.co/products/elasticsearch)的工程项目，然后添加[spring-data-elasticsearch](/https://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/) maven依赖。需要注意的是“*spring-data-elasticsearch依赖的版本要与安装的ElasticSearch对应*”

   ```java
   		<dependency>
   			<groupId>org.springframework.data</groupId>
   			<artifactId>spring-data-elasticsearch</artifactId>
   			<version>${es.version}</version>
   		</dependency>
   ```

3. 添加ES的配置文件：spring-es.xml，配置ElasticsearchTemplate。

   ````xml
   	<!--配置ES连接信息-->
   	<elasticsearch:transport-client id="client" cluster-nodes="192.168.73.1:9300" cluster-     			name="elasticsearch" client-transport-sniff="false" />
   	<!--配置ElasticsearchTemplate-->
       <bean name="elasticsearchTemplate"
             class="org.springframework.data.elasticsearch.core.ElasticsearchTemplate">
             <constructor-arg ref="client" />
       </bean>
   ````

   

### 分页方式一：From&Size





### 分页方式二：scroll

