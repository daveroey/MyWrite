

# JenKins+Spirng Boot+Maven+码云持续化构建

## [jenkins](/https://jenkins.io)安装

笔者在此使用[Docker](/https://docs.docker.com)方式安装jenkins，DockerHub上的[jenkins镜像地址](/https://hub.docker.com/_/jenkins/)

1. 下载jenkins镜像 `docker pull jenkins:2.60.3`

2. 启动jenkins容器：

   ```shell
   docker run --name jenkins -u root -d -p 8080:8080 -p 5000:5000 \
   	-v /home/dave/soft/jenkins/home:/var/jenkins_home \
   	-v /var/run/docker.sock:/var/run/docker.sock \
   	-v /usr/bin/docker:/usr/bin/docker \
   	jenkins:2.60.3
   ```

   > + `-u root` 使 /home/dave/soft/jenkins/home 目录能在容器内访问。
   > + `-v /home/dave/soft/jenkins/home:/var/jenkins_home ` 将jenkins的工作目录挂载到用户目录，方便持久化。
   > + `-v /var/run/docker.sock:/var/run/docker.sock` 和 `-v /usr/bin/docker:/usr/bin/docker` 在docker里面使用doker命令。

## jenkins 配置

1. 访问jenkins宿主机的`8080`端口，创建新的管理员用户。

> 第一次进入需要输入一个默认密码：按照页面的提示找到密码

2. 在管理插件页面安装Maven插件和Webhook插件

![image-20180710084532925](https://upload-images.jianshu.io/upload_images/12521612-bbdb4a648ba41042.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image-20180710084655781](https://upload-images.jianshu.io/upload_images/12521612-146e59b2875b03f7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image-20180710084754188](https://upload-images.jianshu.io/upload_images/12521612-06ef62b5fdf671b3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)





3. 配置全局工具（mave,jdk,git等）

![image-20180710084904094](https://upload-images.jianshu.io/upload_images/12521612-bc73d6dfd5a68b06.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![jdk配置](https://upload-images.jianshu.io/upload_images/12521612-a5299fad3f72e3c6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

> 如果需要修改maven仓库的源地址，可以在`${jenkins_home}/tools/hudson.tasks.Maven_MavenInstallation/maven/conf`下找到maven的settings.xml

## 码云配置

1. 将要构建的项目关联到远程仓库。

![image-20180710090746758](https://upload-images.jianshu.io/upload_images/12521612-adc8f7a08ee1b1ec.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

2.为要构建的仓库添加WebHooks钩子

![image-20180710090911305](https://upload-images.jianshu.io/upload_images/12521612-e99b9d337ffe363c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image-20180710091100787](https://upload-images.jianshu.io/upload_images/12521612-802e4f2d09b400b3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

> 此处的URL: `http://ip:port/generic-webhook-trigger/invoke`,将`ip`和`port`替换成自己的jenkis地址。

## 创建jenkins构建任务

1. 新建任务

![image-20180710091516061](https://upload-images.jianshu.io/upload_images/12521612-d754906e5526e565.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image-20180710091649062](https://upload-images.jianshu.io/upload_images/12521612-e00707ec015465c5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

2. 配置git仓库地址

![image-20180710091840803](https://upload-images.jianshu.io/upload_images/12521612-e20f063cb515b8cf.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

3. 设置触发器条件

![image-20180710092003361](https://upload-images.jianshu.io/upload_images/12521612-d19dceaf97beed6d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

4. 构建任务成功后的后续操作

   ![image-20180710093928275](https://upload-images.jianshu.io/upload_images/12521612-d98b97a1173c7ce3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

   > 配置容器到宿主机的免密登录：
   >
   > + 登录容器 `docker exec -it 容器ID /bin/bash`
   > + 执行 `ssh-keygen -r -rsa` 生成秘钥。
   > + 执行 `ssh-copy-id -i ~/.ssh/id_rsa.pub root@192.168.73.56` 复制公钥到宿主机。

   > 发布说明：
   >
   > + 杀掉已经启动的项目进程，释放端口。
   >
   > + 清理原来的项目目录。
   >
   > + 复制构建任务build生成的jar包到发布目录（jenkins的工作目录挂载到宿主机的目录，直接去宿主机复制）。
   >
   > + 执行启动命令启动jar包，完成发布。
   >
   >   